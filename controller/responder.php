<?php 

include('../db.php');


if  (isset($_GET['id'])) {

    $id = $_GET['id'];
    $query = "SELECT * FROM mensajes WHERE id=$id";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        $nombre = $row['nombre'];
        $description = $row['description'];
      }
    }


if (isset($_POST['responder'])) {
    $id = $_GET['id'];
    $respuesta = $_POST['mensaje'];
  
    $query = "UPDATE mensajes set respuesta = '$respuesta' WHERE id=$id";
    mysqli_query($conn, $query);

    $_SESSION['message'] = 'Has Respondido correctamente el mensaje';
    $_SESSION['message_type'] = 'warning';
    header('Location: ../principal.php');
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Responder Mensaje</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
<div class="container p-4">

  <div class="row">
    <div class="col-md-4 mx-auto">  
      <div class="card card-body">
      <form action="responder.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
         <h5>Responder a <?php echo $nombre; ?></h5>       
        <div class="form-group">
        <textarea name="mensaje" class="form-control mt-4" cols="30" rows="10" placeholder="Mensaje..."></textarea>
        </div>
        <div class="row text-center mt-4">
        <div class="col-sm-5 mx-auto">
        <button class="btn btn-success" name="responder">
          Responder
</button>
        </div>
       
        </div>
        
      </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>