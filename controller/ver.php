<?php 

include('../db.php');


if  (isset($_GET['id'])) {

    $id = $_GET['id'];
    $query = "SELECT * FROM mensajes WHERE id=$id";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);

        $id = $row['id'];
        $nombre = $row['nombre'];
        $telefono = $row['telefono'];
        $email = $row['email'];
        $mensaje = $row['mensaje'];
        $created_at = $row['created_at'];
        $respuesta = $row['respuesta'];

      }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver Mensaje</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
<div class="container p-4">

  <div class="row">
    <div class="col-md-4 mx-auto">  
      <div class="card card-body">

            <h3 class="text-center">Cliente: <?php echo $nombre; ?></h3>
            <h6 class="text-center text-secondary">Telefono: <?php echo $telefono . ' | email: ' . $email; ?></h4>
            <p class="text-center text-secondary">Mensaje: <?php echo $mensaje; ?></p>
            <hr>
            <?php if( $respuesta == null){ ?>
                <div class="row">
                    <div class="col-sm-5 mx-auto">
                    <a href="responder.php?id=<?php echo $id; ?>"class="btn btn-success" name="responder">
                    Responder
                    </a>
                </div>
                </div>
            <?php }else ?>
            <div class="row">
                <div class="col-sm-5 mx-auto">
                    <p class="text-center"><?php echo $respuesta; ?></p>
                </div>
                </div>


        
        </div>
       
        </div>
        
      </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>