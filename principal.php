<?php 

include('db.php');

$query = "SELECT * FROM `mensajes`";
$result = mysqli_query($conn, $query);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mensaje</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <!-- Alerta -->
    <div class="row mt-5">
    <div class="col-sm-5 mx-auto">
    <?php  if (isset( $_SESSION['message'])) { ?>
    <div class="alert alert-<?php  echo $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
        <?php  echo $_SESSION['message'] ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php  session_unset(); }  ?>

    </div>
    </div>
    
    <div class="container mt-5">
        <div class="col-sm-10 mx-auto">
            <table class="table table-striped responsive">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>nombre</th>
                        <th>telefono</th>
                        <th>email</th>
                        <th>mensaje</th>
                        <th>Fecha</th>

                        <th>acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php while ($row = mysqli_fetch_assoc($result)) { ?>

                    <tr>
                        <th><?php echo $row['id'];?></th>
                        <th><?php echo $row['nombre'];?></th>
                        <th><?php echo $row['telefono'];?></th>
                        <th><?php echo $row['email'];?></th>
                        <th><?php echo $row['mensaje'];?></th>
                        <th><?php echo $row['created_at'];?></th>

                        <th>
                            <a href="controller/responder.php?id=<?php echo $row['id'];?>" class="btn btn-primary"><i class="bi-reply"></i></a>
                            <a href="controller/ver.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-eye"></i></a>
                            <a href="controller/eliminar.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-trash"></i></a>

                        </th>

                    </tr>



                    <?php   }?>

                </tbody>
            </table>
        </div>
    </div>

</body>

</html>